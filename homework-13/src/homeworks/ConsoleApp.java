package homeworks;
import homeworks.Controllers.FamilyController;

import java.io.Serializable;
import java.util.*;
import java.util.function.Predicate;

public class ConsoleApp implements Serializable {

    ArrayList<String> commandOptions = new ArrayList<String>() {{
        add("1. Отобразить весь список семей (отображает список всех семей с индексацией, начинающейся с 1)");
        add("2. Отобразить список семей, где количество людей больше заданного");
        add("3. Отобразить список семей, где количество людей меньше заданного");
        add("4. Подсчитать количество семей, где количество членов равно");
        add("5. Создать новую семью");
        add("6. Удалить семью по индексу семьи в общем списке");
        add("7. Редактировать семью по индексу семьи в общем списке ");
        add("8. Удалить всех детей старше возраста (во всех семьях удаляются дети старше указанного возраста - будем считать, что они выросли)");
        add("9. Записать семьи с локальной бд в файл");
        add("10. Добавить семьи с файла в локальную бд");
    }};

    FamilyController familyController = new FamilyController();
    transient Scanner in = new Scanner(System.in);
    transient Scanner externalIn = new Scanner(System.in);
    Boolean isExit = false;

    public void startConsoleApp () {

        commandOptions.forEach(System.out::println);
        System.out.println("Enter any number from the list (without dot or any other symbol): ");
        System.out.println("Or enter 'exit' to leave :)");

        do {
            String mainMenu = externalIn.nextLine().trim().toUpperCase();

            switch (mainMenu) {
                case ("1"): {
                    displayFamilies();
                    break;
                }
                case ("2"): {
                    getFamiliesBiggerThan();
                    break;
                }
                case ("3"): {
                    getFamiliesLessThan();
                    break;
                }
                case ("4"): {
                    getEqualFamilies();
                    break;
                }
                case ("5"): {
                    createNewFamily();
                    break;
                }
                case ("6"): {
                    deleteFamilyByIndex();
                    break;
                }
                case ("7"): {
                    editFamilyByIndex();
                    break;
                }
                case ("8"): {
                    deleteAllChildOlderThan();
                    break;
                }
                case ("9"): {
                    writeFamiliesToFile();
                    break;
                }
                case ("10") : {
                    loadData();
                    break;
                }
                case ("EXIT"): {
                    isExit = true;
                }
                default:
                    System.out.println("There is no such option");
            }

        } while (!isExit);
        System.out.println("Goodbye :)");
    }

    private void displayFamilies() {
        familyController.displayAllFamilies();
    }

    private void getFamiliesBiggerThan() {
        try {
            System.out.print("Введите число: ");
            int number = in.nextInt();
            Predicate<Family> familyPredicate = f -> f.countFamily() > number;
            boolean familyCondition = familyController.getAllFamilies().stream().noneMatch(familyPredicate);

            if (familyController.getAllFamilies().size() != 0) {
                if (familyCondition) {
                    System.out.println("There is no such family");
                } else {
                    familyController.getFamiliesBiggerThan(number);
                }
            } else {
                System.out.println("There are no families in the database");
            }
        } catch (InputMismatchException e) {
            in.nextLine();
            System.out.println("Ошибка: Вероятно вы ввели строку, где ожидалось число");
            commandOptions.forEach(System.out::println);
        }
    }

    private void getFamiliesLessThan() {
        try {
            System.out.print("Введите число: ");
            int number = in.nextInt();
            Predicate<Family> familyPredicate = f -> f.countFamily() < number;

            boolean familyCondition = familyController.getAllFamilies().stream().noneMatch(familyPredicate);
            if (familyController.getAllFamilies().size() != 0) {
                if (familyCondition) {
                    System.out.println("There is no such family");
                } else {
                    familyController.getFamiliesLessThan(number);
                }
            } else {
                System.out.println("There are no families in the database");
            }
        } catch (InputMismatchException e) {
            in.nextLine();
            System.out.println("Ошибка: Вероятно вы ввели строку, где ожидалось число");
            commandOptions.forEach(System.out::println);
        }
    }

    private void getEqualFamilies() {
        try {
            System.out.print("Введите число: ");
            int number = in.nextInt();
            Predicate<Family> familyPredicate = f -> f.countFamily() == number;
            boolean familyCondition = familyController.getAllFamilies().stream().noneMatch(familyPredicate);
            if (familyController.getAllFamilies().size() != 0) {
                if (familyCondition) {
                    System.out.println("There is no such family");
                } else {
                    familyController.getAllFamilies()
                            .stream()
                            .filter(family -> family.countFamily() == number)
                            .forEach(System.out::println);
                }
            } else {
                System.out.println("There are no families in the database");
            }
        } catch (InputMismatchException e) {
            in.nextLine();
            System.out.println("Ошибка: Вероятно вы ввели строку, где ожидалось число");
            commandOptions.forEach(System.out::println);
        }
    }

    private void createNewFamily() {
        try {
            System.out.print("Введите имя матери: ");
            String motherName = in.nextLine().trim();
            if (motherName.matches("\\d+")) {
                System.out.println("Shouldn't contain a number");
                return;
            }
            System.out.print("Введите фамилию матери: ");
            String motherSurname = in.nextLine().trim();
            if (motherSurname.matches("\\d+")) {
                System.out.println("Shouldn't contain a number");
                return;
            }
            System.out.print("Введите год рождения матери: ");
            int motherBirthYear = in.nextInt();
            System.out.print("Введите месяц рождения матери: ");
            int motherBirthMonth = in.nextInt();
            System.out.print("Введите день рождения матери: ");
            int motherBirthDay = in.nextInt();
            System.out.print("Введите IQ матери: ");
            int motherIq = in.nextInt();
            String motherBirthDate = "" + motherBirthDay + "/" + motherBirthMonth + "/" + motherBirthYear + "";

            Woman mom = new Woman(motherName, motherSurname, motherBirthDate, motherIq);

            in.nextLine();

            System.out.print("\nВведите имя отца: ");
            String fatherName = in.nextLine().trim();
            if (fatherName.matches("\\d+")) {
                System.out.println("Shouldn't contain a number");
                return;
            }
            System.out.print("Введите фамилию отца: ");
            String fatherSurname = in.nextLine().trim();
            if (fatherSurname.matches("\\d+")) {
                System.out.println("Shouldn't contain a number");
                return;
            }
            System.out.print("Введите год рождения отца: ");
            int fatherBirthYear = in.nextInt();
            System.out.print("Введите месяц рождения отца: ");
            int fatherBirthMonth = in.nextInt();
            System.out.print("Введите день рождения отца: ");
            int fatherBirthDay = in.nextInt();
            System.out.print("Введите IQ отца: ");
            int fatherIq = in.nextInt();
            String fatherBirthDate = "" + fatherBirthDay + "/" + fatherBirthMonth + "/" + fatherBirthYear + "";

            Man dad = new Man(fatherName, fatherSurname, fatherBirthDate, fatherIq);

            familyController.createNewFamily(dad, mom);
            System.out.println("Family is created");
        } catch (InputMismatchException e) {
            in.nextLine();
            System.out.println("Ошибка: Вероятно вы ввели строку, где ожидалось число");
            commandOptions.forEach(System.out::println);
        }
    }

    private void deleteFamilyByIndex() {
        try {
            System.out.print("Введите число: ");
            int number = in.nextInt();
            familyController.deleteFamilyByIndex(number);
            System.out.println("Family is deleted");
        } catch (InputMismatchException e) {
            in.nextLine();
            System.out.println("Ошибка: Вероятно вы ввели строку, где ожидалось число");
            commandOptions.forEach(System.out::println);
        }
    }

    private void editFamilyByIndex() {
        System.out.println("1. Родить ребенка");
        System.out.println("2. Усыновить ребенка");
        System.out.println("3. Вернуться в главное меню");
        String chooseNum = in.nextLine();
        if (!chooseNum.matches("\\d+")) {
            System.out.println("You have to pick a number");
            return;
        }
        try {
            switch (chooseNum) {
                case ("1"): {
                    System.out.print("Введите ID семьи: ");
                    int familyIndex = in.nextInt();
                    in.nextLine();
                    System.out.print("Введите имя девочки: ");
                    String womanName = in.nextLine();
                    if (womanName.matches("\\d+")) {
                        System.out.println("Shouldn't contain a number");
                        return;
                    }
                    System.out.print("Введите имя мальчика: ");
                    String manName = in.nextLine();
                    if (manName.matches("\\d+")) {
                        System.out.println("Shouldn't contain a number");
                        return;
                    }
                    Family family = familyController.getFamilyById(familyIndex);
                    familyController.bornChild(family, womanName, manName);
                    System.out.println("Поздравляем у вас родился ребенок :)");
                    break;
                }
                case ("2"): {
                    System.out.print("Введите ID семьи: ");
                    int familyIndex = in.nextInt();
                    in.nextLine();
                    System.out.println("Введите данные ребенка");
                    System.out.print("Введите имя: ");
                    String childName = in.nextLine();
                    if (childName.matches("\\d+")) {
                        System.out.println("Shouldn't contain a number");
                        return;
                    }
                    System.out.print("Введите фамилию: ");
                    String childSurname = in.nextLine();
                    if (childSurname.matches("\\d+")) {
                        System.out.println("Shouldn't contain a number");
                        return;
                    }
                    System.out.print("Введите дату рождения (формат - DD/MM/YYYY): ");
                    String childBirthDate = in.nextLine();
                    if (!childBirthDate.matches("\\d{2}/\\d{2}/\\d{4}")) {
                        System.out.println("Follow the format");
                        return;
                    }
                    System.out.print("Введите iq: ");
                    int childIq = in.nextInt();
                    Human child = new Human(childName, childSurname, childBirthDate, childIq);
                    Family family = familyController.getFamilyById(familyIndex);
                    familyController.adoptChild(family, child);
                    System.out.println("Поздравляем вы усыновили ребенка :)");
                    break;
                }
                case ("3"): {
                    commandOptions.forEach(System.out::println);
                    break;
                }
                default:
                    System.out.println("There is no such option");
                    break;
            }
        } catch (InputMismatchException | IndexOutOfBoundsException e) {
            in.nextLine();
            System.out.println("Ошибка: Вероятно вы ошиблись с ID семьи или ввели строку, где ожидалось число");
            commandOptions.forEach(System.out::println);
        } catch (FamilyOverflowException e) {
            System.out.println("Исключение: вы не можете родить/усыновить ребенка, когда в семье 5 и более человек");
        }
    }

    private void deleteAllChildOlderThan() {
        try {
            System.out.print("Введите возраст: ");
            int age = in.nextInt();
            familyController.deleteAllChildrenOlderThen(age);
            System.out.println("Это очень странно, но если такие дети были, то вы их удалили, поздравляем :)");
        } catch (InputMismatchException e) {
            in.nextLine();
            System.out.println("Ошибка: Вероятно вы ввели строку, где ожидалось число");
            commandOptions.forEach(System.out::println);
        }
    }

    private void loadData() {
        familyController.loadData(familyController.getAllFamilies());
    }

    public void writeFamiliesToFile() {
        familyController.writeFamilyToFile(familyController.getAllFamilies());
    }
}
