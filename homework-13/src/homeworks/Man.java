package homeworks;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;

public final class Man extends Human {

    public Man (String name, String surname) {
        super(name, surname);
    }

    public Man(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    public Man (String name, String surname, String birthDate, int iq, LinkedHashMap<String, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public Man (String name, String surname, String birthDate, int iq, LinkedHashMap<String, String> schedule, Family family) {
        super(name, surname, birthDate, iq, schedule, family);
    }

    @Override
    String greetPet() {
        if (this.family != null && this.family.getPet() != null) {
            return "Привет, " + this.family.getPet() + ", я дома";
        } else {
            return "У меня нет питомца";
        }
    }

    String eatNuggets () {
        return "daaamn! u ate 9 nuggets";
    }
}
