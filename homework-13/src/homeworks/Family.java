package homeworks;

import homeworks.Controllers.FamilyController;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Family implements Serializable {

    private Human mother;
    public Human father;
    private List<Human> children;
    private HashSet<Pet> pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.mother.setFamily(this);
        this.father = father;
        this.father.setFamily(this);
        this.pet = new HashSet<>();
        this.children = new ArrayList<>();
        FamilyController.saveFamily(this);
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public HashSet<Pet> getPet() {
        return this.pet;
    }

    String getEveryPet(String parameter) {
        StringBuilder allPets = new StringBuilder();

        switch (parameter){
            case "nickname":
                for (Pet pet : this.getPet()) {
                    allPets.append(pet.getNickname()).append("; ");
                }
                break;
            case "trickLevel":
                for (Pet pet : this.getPet()) {
                    allPets.append(pet.getTrickLevel() > 50 ? "очень хитрый; " : "почти не хитрый; ");
                }
                break;
            case "age":
                for (Pet pet : this.getPet()) {
                    allPets.append(pet.getAge()).append("; ");
                }
                break;
            default:
                return "no val";
        }
        return allPets.toString();
    }

    public void addPet(Pet pet) {
        this.pet.add(pet);
    }

    public boolean addChild(Human children) {
        int beforeAddSize = this.children.size();
        this.children.add(children);
        int afterAddSize = this.children.size();
        return beforeAddSize + 1 == afterAddSize;
    }

    boolean deleteChild(Human children) {
        int beforeRemoveSize = this.children.size();
        this.children.remove(children);
        int afterRemoveSize = this.children.size();
        return beforeRemoveSize - 1 == afterRemoveSize;
    }

    boolean deleteChild(int index) {
        int beforeRemoveSize = this.children.size();
        this.children.remove(index);
        int afterRemoveSize = this.children.size();
        return beforeRemoveSize - 1 == afterRemoveSize;
    }

    public String prettyFormat() {
        String kids = this.children.stream().map(e -> e.prettyFormat(this)).collect(Collectors.joining());
        String pets = this.pet.stream().map(Pet::prettyFormat).collect(Collectors.joining());

        return "family: \n" +
                "      mother: " + this.mother.prettyFormat(this) + ",\n" +
                "      father: " + this.father.prettyFormat(this) + ",\n" +
                "      children: \n" +
                kids +
                "   pets: " + "[ " + pets + "]";
    }

    public int countFamily () {
        return 2 + this.children.size();
    }

    @Override
    public String toString() {
        return "Family info - { mother=" + this.mother + ", father=" + this.father +
                ", child=" + this.children.toString() + ", pet=" + this.pet.toString() + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return mother.equals(family.mother) &&
                father.equals(family.father) &&
                Objects.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pet);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Deleted obj: " + this.mother + ", " + this.father + "; It's hashcode: " + this.hashCode() );
        super.finalize();
    }
}
