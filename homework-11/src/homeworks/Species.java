package homeworks;

public enum Species {
    BULLDOG,
    LABRADOR,
    DALMATIAN,
    CLOWN,
    GOLD,
    CLEANER,
    PLAYER,
    BOMBAY,
    BENGAL,
    UNKNOWN
}
