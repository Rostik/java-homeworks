package homeworks;

class Fish extends Pet {

    Fish (String nickname, int age, int trickLevel, Species species) {
        super(nickname, age, trickLevel);
        this.species = species;
    };

    String respond () {
        return "Я - рыбка" + this.nickname + ", где корм?";
    }
}