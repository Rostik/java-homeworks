package homeworks;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.LinkedHashMap;

import static org.junit.Assert.*;

public class HumanTest {

    @Test
    public void testToString() {
        Human human = new Human( "Amanda", "Docker", "01/01/1979", 100,
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to the pool");
                    put(DayOfWeek.TUESDAY.name(), "do some stuff");
                    put(DayOfWeek.WEDNESDAY.name(), "go to the market");
                    put(DayOfWeek.THURSDAY.name(), "go to a fast-food");
                    put(DayOfWeek.FRIDAY.name(), "participate in rap");
                    put(DayOfWeek.SATURDAY.name(), "play with doggy");
                }}
                );
        String expected = "Human{name=Amanda, surname=Docker, birthDay=01/01/1979, iq=100," +
                " schedule={SUNDAY=sleep all day long, MONDAY=go to the pool, TUESDAY=do some stuff," +
                " WEDNESDAY=go to the market, THURSDAY=go to a fast-food, FRIDAY=participate in rap," +
                " SATURDAY=play with doggy}}";

        Assert.assertEquals(expected, human.toString());
    }
}