package homeworks;

class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Human mother;
    private Human father;
    private String [][] schedule = new String[7][2];

    Human () {

    }

    Human (String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    Human (String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    Human (String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String [][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    String greetPet () {
        return "Привет, " + this.pet.nickname;
    }

    String describePet () {
        return "У меня есть - " + this.pet.species +
                " ему " + this.pet.age + ", он " +
                (this.pet.trickLevel > 50 ? "очень хитрый" : "почти не хитрый");
    }

    @Override
    public String toString() {
        return "Human{name=" + this.name + "," +
                " surname=" + this.surname +
                ", year=" + this.year + ", iq=" + this.iq +
                ", mother=" + this.mother.name + ", father=" + this.father.name +
                ", pet=" + this.pet + "}";
    }
}
