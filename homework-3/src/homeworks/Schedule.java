//        Задание
//        Написать консольную программу "планирощик задач на неделю".
//
//        Технические требования:
//
//
//        Создать двумерный массив строк размерностью 7х2
//        String[][] scedule = new String[7][2]
//
//
//        Заполните матрицу значениями день недели: главное задание на данный день:
//        scedule[0][0] = "Sunday";
//        scedule[0][1] = "do home work";
//        scedule[1][0] = "Monday";
//        scedule[1][1] = "go to courses; watch a film";
//
//
//        Используя цикл и оператор switch, запросите у пользователя ввести день недели в консоль и, в зависимости от ввода:
//
//        программа: Please, input the day of the week:
//        пользователь вводит корректный день недели (f.e. Monday)
//        программа выводит на экран Your tasks for Monday: go to courses; watch a film.; программа идет на следующую итерацию;
//        программа: Please, input the day of the week:
//        пользователь вводит некорректный день недели (f.e. %$=+!11=4)
//        программа выводит на экран Sorry, I don't understand you, please try again.; программа идет на следующую итерацию до успешного ввода;
//        программа: Please, input the day of the week:
//        пользователь выводит команду выхода exit
//        программа выходит из цикла и корректно завершает работу.
//
//
//
//        Задание должно быть выполнено ипспользуя массивы (НЕ используйте интерфейсы List, Set, Map).
//        Учтите: программа должна принимать команды как в нижнем регистре (monday) так и в верхнем (MONDAY) и учитывать, что пользователь мог случайно после дня недели ввести пробел.

package homeworks;

import java.util.Scanner;

public class Schedule {

    private static void getRoutine () {

        String[][] schedule = {
                {"Sunday", "do homework"},
                {"Monday", "Go to courses"},
                {"Tuesday", "Watch a movie"},
                {"Wednesday", "Visit my friends"},
                {"Thursday", "Go to gym"},
                {"Friday", "Drink some beer"},
                {"Saturday", "Have a hangover"}
        };

        Scanner scanner = new Scanner(System.in);

        while (true) {

            System.out.print("Please, enter the day of the week: ");
            String enteredDay = scanner.nextLine();

            switch (enteredDay.toLowerCase().trim()) {
                case "sunday" : System.out.println(schedule[0][1]);
                break;
                case "monday" : System.out.println("Your tasks for Monday: " + schedule[1][1]);
                break;
                case "tuesday" : System.out.println(schedule[2][1]);
                break;
                case "wednesday" : System.out.println(schedule[3][1]);
                break;
                case "thursday" : System.out.println(schedule[4][1]);
                break;
                case "friday" : System.out.println(schedule[5][1]);
                break;
                case "saturday" : System.out.println(schedule[6][1]);
                break;
                case "exit" : return;
                default: System.out.println("Sorry, I don't understand you, please try again");
            }

        }

    }

    public static void main(String[] args) {
        getRoutine();
    }

}
