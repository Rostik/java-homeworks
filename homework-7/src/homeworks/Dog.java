package homeworks;

class Dog extends Pet implements Foulable {

    Dog (String nickname, int age, int trickLevel, String [] habits, Species species) {
        super(nickname, age, trickLevel, habits);
        this.species = species;
    };

    String respond () {
        return "Привет, хозяин. Я - " + this.nickname + ", я соскучился!";
    }

    @Override
    public String foul() {
        return "Нужно хорошо замести следы...";
    }
}
