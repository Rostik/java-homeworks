package homeworks;

import java.util.Arrays;
import java.util.Objects;

abstract class Pet {

    String nickname;
    private int age;
    private int trickLevel;
    private String [] habits;
    Species species = Species.UNKNOWN;

    String getNickname() {
        return nickname;
    }

    int getAge() {
        return age;
    }

    int getTrickLevel() {
        return trickLevel;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setTrickLevel(int trickLevel) {
        if (trickLevel >= 1 && trickLevel <= 100) {
            this.trickLevel = trickLevel;
        } else {
            System.out.println("Ошибка! Уровень хитрости не может быть отрицательным или больше 100");
        }
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    Pet (String nickname, int age, int trickLevel, String [] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    String eat () {
        return "Я кушаю!";
    }

    abstract String respond();

    @Override
    public String toString() {
        return "{nickname=" + this.nickname
                + ", age=" + this.age + ", trickLevel=" +
                this.trickLevel + ", habits=" + Arrays.toString(this.habits) + " species=" + this.species.name() + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                trickLevel == pet.trickLevel &&
                nickname.equals(pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nickname, age, trickLevel);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Deleted obj: " + this.nickname + "; It's hashcode: " + this.hashCode() );
        super.finalize();
    }
}
