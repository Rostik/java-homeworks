package homeworks;

class RoboCat extends Pet implements Foulable {

    RoboCat (String nickname, int age, int trickLevel, String [] habits, Species species) {
        super(nickname, age, trickLevel, habits);
        this.species = species;
    };

    String respond () {
        return "Привет, хозяин. Я, робо-кот - " + this.nickname + ", я сделал все дела!";
    }

    @Override
    public String foul() {
        return "Нужно хорошо замести следы...";
    }
}