package homeworks;

class Fish extends Pet {

    Fish (String nickname, int age, int trickLevel, String [] habits, Species species) {
        super(nickname, age, trickLevel, habits);
        this.species = species;
    };

    String respond () {
        return "Я - рыбка" + this.nickname + ", где корм?";
    }
}