package homeworks;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class HumanTest {

    @Test
    public void testToString() {
        Human human = new Human( "Amanda", "Docker", 1979, 100,
                new String[][] {
                        {DayOfWeek.SUNDAY.name(), "sleep all day long"},
                        {DayOfWeek.MONDAY.name(), "go to the dentist"},
                        {DayOfWeek.TUESDAY.name(), "read book"},
                        {DayOfWeek.WEDNESDAY.name(), "go to the swimming pool"},
                        {DayOfWeek.THURSDAY.name(), "go to kfc"},
                        {DayOfWeek.FRIDAY.name(), "participate in music"},
                        {DayOfWeek.SATURDAY.name(), "play piano"}
                });
        String expected = "Human{name=Amanda, surname=Docker, year=1979," +
                        " iq=100, schedule=[[SUNDAY, sleep all day long]," +
                        " [MONDAY, go to the dentist], [TUESDAY, read book]," +
                        " [WEDNESDAY, go to the swimming pool]," +
                        " [THURSDAY, go to kfc], [FRIDAY, participate in music]," +
                        " [SATURDAY, play piano]]";
        Assert.assertEquals(expected, human.toString());
    }
}