package homeworks;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class PetTest {

    @Test
    public void testToString() {
        Pet pet = new Pet(Species.DOG, "Beebop",
                3, 55,
                new String[]{"barking", "eating", "sleeping"});
        String expected = "Bulldog{nickname=Beebop, age=3, trickLevel=55, habits=[barking, eating, sleeping]}";
        Assert.assertEquals(expected, pet.toString());
    }
}