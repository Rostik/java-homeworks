package homeworks;

import java.util.Random;

public class Main {

    private static int getRandomNum () {
        Random random = new Random();
        return random.nextInt(101);
    }
    public static void main(String[] args) {
        Pet doggy = new Pet(
                Species.DOG, "Beebop",
                3, Main.getRandomNum(),
                new String[]{"barking", "eating", "sleeping"}
        );
        Human dad = new Human("Robert", "Docker", 1975,  Main.getRandomNum(),
                new String[][] {
                        {DayOfWeek.SUNDAY.name(), "sleep all day long"},
                        {DayOfWeek.MONDAY.name(), "go to work"},
                        {DayOfWeek.TUESDAY.name(), "read book"},
                        {DayOfWeek.WEDNESDAY.name(), "go to the swimming pool"},
                        {DayOfWeek.THURSDAY.name(), "go to kfc"},
                        {DayOfWeek.FRIDAY.name(), "participate in music"},
                        {DayOfWeek.SATURDAY.name(), "play video games"}
                }
                );
        Human mom = new Human(
                "Amanda", "Docker", 1979, Main.getRandomNum(),
                new String[][] {
                        {DayOfWeek.SUNDAY.name(), "sleep all day long"},
                        {DayOfWeek.MONDAY.name(), "go to work"},
                        {DayOfWeek.TUESDAY.name(), "read book"},
                        {DayOfWeek.WEDNESDAY.name(), "go to the swimming pool"},
                        {DayOfWeek.THURSDAY.name(), "go to kfc"},
                        {DayOfWeek.FRIDAY.name(), "participate in music"},
                        {DayOfWeek.SATURDAY.name(), "play piano"}
                }
        );
        Human son = new Human(
                "Andy", "Docker", 2001, Main.getRandomNum(),
                new String[][] {
                        {DayOfWeek.SUNDAY.name(), "sleep all day long"},
                        {DayOfWeek.MONDAY.name(), "go to school"},
                        {DayOfWeek.TUESDAY.name(), "read book"},
                        {DayOfWeek.WEDNESDAY.name(), "go to the swimming pool"},
                        {DayOfWeek.THURSDAY.name(), "go to kfc"},
                        {DayOfWeek.FRIDAY.name(), "participate in music"},
                        {DayOfWeek.SATURDAY.name(), "play video games"}
                }
        );
        Human son2 = new Human(
                "Mikey", "Docker", 2002, Main.getRandomNum(),
                new String[][] {
                        {DayOfWeek.SUNDAY.name(), "sleep all day long"},
                        {DayOfWeek.MONDAY.name(), "go to school"},
                        {DayOfWeek.TUESDAY.name(), "read book"},
                        {DayOfWeek.WEDNESDAY.name(), "go to the gym"},
                        {DayOfWeek.THURSDAY.name(), "go to kfc"},
                        {DayOfWeek.FRIDAY.name(), "participate in music"},
                        {DayOfWeek.SATURDAY.name(), "play video games"}
                }
        );
        Family fam = new Family(mom, dad);

        fam.addChild(son);

        System.out.println(doggy.eat());
        System.out.println(doggy.respond());
        System.out.println(doggy.foul());
        System.out.println(doggy.toString());

        System.out.println(son.greetPet());
        System.out.println(son.describePet());
        System.out.println(mom.toString());

        System.out.println(mom.hashCode());
        System.out.println(fam.hashCode());
        System.out.println(dad.equals(mom));

        System.out.println(fam.countFamily(fam));
        System.out.println(fam.deleteChild(son));
        System.out.println(fam.countFamily(fam));
        System.out.println(fam.toString());
        son2.setFamily(fam);
        System.out.println(fam.toString());
        System.out.println(fam.getPet());

//        for (int i = 0; i < 500000; i++) {
//            new Human("Neo", "Matrix", 1919, Main.getRandomNum(),
//                    new String[][]{
//                            {"Sunday", "sleep all day long"},
//                            {"Monday", "take pill"},
//                            {"Tuesday", "read book"},
//                            {"Wednesday", "go to the swimming pool"},
//                            {"Thursday", "go to kfc"},
//                            {"Friday", "participate in music"},
//                            {"Saturday", "play video games"}
//                    }
//            );
//        }
    }
}