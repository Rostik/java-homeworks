package homeworks;

import java.util.Arrays;
import java.util.Objects;

class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private String [][] schedule = new String[7][2];
    private Family family;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    Family getFamily() {
        return family;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    void setFamily(Family family) {
        this.family = family;
    }

    Human (String name, String surname, int year, int iq, String [][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    Human (String name, String surname, int year, int iq, String [][] schedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.family = family;
    }

    String greetPet () {
        return this.family != null ? "Привет, " + this.family.getPet().getNickname() : "У меня нет пса";
    }

    String describePet () {
        return this.family != null ? "У меня есть - " + this.family.getPet().getSpecies() +
                " ему " + this.family.getPet().getAge() + ", он " +
                (this.family.getPet().getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый")
                : "У меня нет пса";
    }

    @Override
    public String toString() {
        return "Human{name=" + this.name +
                ", surname=" + this.surname +
                ", year=" + this.year +
                ", iq=" + this.iq +
                ", schedule=" + Arrays.deepToString(this.schedule);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                iq == human.iq &&
                name.equals(human.name) &&
                surname.equals(human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq);
    }
}
