package homeworks;

import java.util.Arrays;
import java.util.Objects;

class Pet {

    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String [] habits;

    String getSpecies() {
        return species;
    }

    String getNickname() {
        return nickname;
    }

    int getAge() {
        return age;
    }

    int getTrickLevel() {
        return trickLevel;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setTrickLevel(int trickLevel) {
        if (trickLevel >= 1 && trickLevel <= 100) {
            this.trickLevel = trickLevel;
        } else {
            System.out.println("Ошибка! Уровень хитрости не может быть отрицательным или больше 100");
        }
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    Pet (String species, String nickname, int age, int trickLevel, String [] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    String eat () {
        return "Я кушаю!";
    }

    String respond () {
        return "Привет, хозяин. Я - " + this.nickname + ", я соскучился!";
    }

    String foul () {
        return "Нужно хорошо замести следы...";
    }

    @Override
    public String toString() {
        return this.species + "{nickname=" + this.nickname
                + ", age=" + this.age + ", trickLevel=" +
                this.trickLevel + ", habits=" + Arrays.toString(this.habits) + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                trickLevel == pet.trickLevel &&
                species.equals(pet.species) &&
                nickname.equals(pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel);
    }
}
