package homeworks;

import java.util.Arrays;
import java.util.Objects;

public class Family {

    private Human mother;
    private Human father;
    private Human [] children;
    private Pet pet;
    private Family family;

    Family (Human mother, Human father) {
        this.mother = mother;
        this.mother.setFamily(this);
        this.father = father;
        this.father.setFamily(this);
        this.children = new Human[0];
    }

    Family (Human mother, Human father, Pet pet) {
        this.mother = mother;
        this.mother.setFamily(this);
        this.father = father;
        this.father.setFamily(this);
        this.pet = pet;
        this.children = new Human[0];
    }

    Pet getPet() {
        return pet;
    }

    void addChild (Human children) {
        Human [] newChildrenArr = new Human [this.children.length + 1];
        System.arraycopy(this.children, 0, newChildrenArr, 0, this.children.length);
        children.setFamily(this);
        newChildrenArr[this.children.length] = children;
        this.children = newChildrenArr;
    }

    boolean deleteChild(Human children) {
        Human [] newChildrenArr = new Human[this.children.length - 1];
        boolean isDeleted = false;
        children.setFamily(null);
        for(int i = 0; i < this.children.length; i++) {
            if(this.children[i].equals(children)) {
                for(int j = i; j < newChildrenArr.length; j++) {
                    newChildrenArr[j] = this.children[j + 1];
                }
                for(int j = 0; j < i; j++) {
                    newChildrenArr[j] = this.children[j];
                }
                isDeleted = true;
            }
        }
        this.children = newChildrenArr;
        return isDeleted;
    }

    int countFamily (Family family) {
        return 2 + (this.pet != null ? 1 : 0) + this.children.length;
    }

    @Override
    public String toString() {
        return "Family info - { mother=" + this.mother + ", father=" + this.father +
                ", child=" + Arrays.deepToString(this.children) + ", pet=" + this.pet + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family1 = (Family) o;
        return mother.equals(family1.mother) &&
                father.equals(family1.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }
}
