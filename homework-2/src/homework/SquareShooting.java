//        Дан квадрат 5х5, где программа случайным образом ставит цель.
//        Перед началом игры на экран выводится текст: All set. Get ready to rumble!.
//        Сам процесс игры обрабатывается в бесконечном цикле.
//        Игроку предлагается ввести линию для стрельбы; программа проверяет что бьло введено число, и введенная линия находится в границах игрового поля (1-5). В случае, если игрок ошибся предлагает ввести число еще раз.
//        Игроку предлагается ввести столбик для стрельбы (должен проходить аналогичную проверку).
//        После каждого выстрела необходимо отображать обновленное игровое поле в консоли. Клетки, куда игрок уже стрелял, необходимо отметить как *.
//        Игра заканчивается при поражении цели. В конце игры вывести в консоль фразу You have won!, а также игровое поле. Пораженную цель отметить как x.
//        Задание должно быть выполнено ипспользуя массивы (НЕ используйте интерфейсы List, Set, Map).

package homework;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class SquareShooting {

        private static void shootOn () {

            int min = 1;
            int max = 5;
            int diff = max - min;
            Random random = new Random();
            int pickedShootLine = random.nextInt(diff + 1);
            pickedShootLine += min;
            int pickedShootColumn = random.nextInt(diff + 1);
            pickedShootColumn += min;

            System.out.println(pickedShootLine + " - picked line");
            System.out.println(pickedShootColumn + " - picked column");

            Object[][] battleField = {
                    { "0 |", "1 |", "2 |", "3 |", "4 |", "5 |" },
                    { "1 |", "- |", "- |", "- |", "- |", "- |" },
                    { "2 |", "- |", "- |", "- |", "- |", "- |" },
                    { "3 |", "- |", "- |", "- |", "- |", "- |" },
                    { "4 |", "- |", "- |", "- |", "- |", "- |" },
                    { "5 |", "- |", "- |", "- |", "- |", "- |" }
            };

            System.out.println("All set. Get ready to rumble!");

            Scanner lineNumber = new Scanner(System.in);
            int enteredLine = 0;
            boolean isNumericLine = false;

            Scanner columnNumber = new Scanner(System.in);
            int enteredColumn = 0;
            boolean isNumericColumn = false;

            while (true) {

                while(!isNumericLine)
                    try {
                        System.out.print("Enter shoot line from 1 to 5: ");
                        enteredLine = lineNumber.nextInt();
                        lineNumber.nextLine();
                        isNumericLine = true;
                    } catch(InputMismatchException ime) {
                        System.out.println("Your value is not a number try again");
                        lineNumber.nextLine();
                    }

                while(!isNumericColumn)
                    try {
                        System.out.print("Enter shoot column from 1 to 5: ");
                        enteredColumn = columnNumber.nextInt();
                        columnNumber.nextLine();
                        isNumericColumn = true;
                    } catch(InputMismatchException ime) {
                        System.out.println("Your value is not a number try again");
                        columnNumber.nextLine();
                    }

                while ((enteredLine < 1 || enteredColumn < 1) || (enteredLine > 5 || enteredColumn > 5)) {
                    System.out.println("Wrong numbers, try again");

                    System.out.print("Enter shoot line from 1 to 5: ");
                    enteredLine = lineNumber.nextInt();

                    System.out.print("Enter shoot column from 1 to 5: ");
                    enteredColumn = columnNumber.nextInt();
                }

                if (pickedShootLine == enteredLine && pickedShootColumn == enteredColumn) {
                    battleField[enteredLine][enteredColumn] = "x |";

                    for (int i = 0; i < battleField.length; i++) {
                        for (int j = 0; j < battleField[i].length; j++) {
                            System.out.print(battleField[i][j] + "\t");
                        }
                        System.out.println();
                    }
                    System.out.println("You have won, congrats!");

                    break;

                } else if (pickedShootLine != enteredLine || pickedShootColumn != enteredColumn) {
                    battleField[enteredLine][enteredColumn] = "* |";

                    for (int i = 0; i < battleField.length; i++) {
                        for (int j = 0; j < battleField[i].length; j++) {
                            System.out.print(battleField[i][j] + "\t");
                        }
                        System.out.println();
                    }

                    System.out.println("Woops u missed, try again");

                    System.out.print("Enter shoot line from 1 to 5: ");
                    enteredLine = lineNumber.nextInt();

                    System.out.print("Enter shoot column from 1 to 5: ");
                    enteredColumn = columnNumber.nextInt();
                }
            }

        }

        public static void main(String[] args) {
            shootOn();
        }
}
