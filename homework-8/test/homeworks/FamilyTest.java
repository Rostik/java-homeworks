package homeworks;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import static org.junit.Assert.*;

public class FamilyTest {

    @Test
    public void testToString() {
        Family family = new Family( new Human(
         "Amanda", "Docker", 1979, 85,
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to the work");
                    put(DayOfWeek.TUESDAY.name(), "do some stuff");
                    put(DayOfWeek.WEDNESDAY.name(), "go to the market");
                    put(DayOfWeek.THURSDAY.name(), "go to a fast-food");
                    put(DayOfWeek.FRIDAY.name(), "participate in rap");
                    put(DayOfWeek.SATURDAY.name(), "play with doggy");
                }}), new Human("Robert", "Docker", 1975,  75,
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to the work");
                    put(DayOfWeek.TUESDAY.name(), "read book");
                    put(DayOfWeek.WEDNESDAY.name(), "play football");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in tennis");
                    put(DayOfWeek.SATURDAY.name(), "play video games");
                }}
        ));
        String expected = "Family info - { " +
                "mother=Human{name=Amanda, surname=Docker, year=1979, iq=85, " +
                "schedule={SUNDAY=sleep all day long, MONDAY=go to the work, TUESDAY=do some stuff, " +
                "WEDNESDAY=go to the market, THURSDAY=go to a fast-food, FRIDAY=participate in rap, " +
                "SATURDAY=play with doggy}}, " +
                "father=Human{name=Robert, surname=Docker, year=1975, iq=75, " +
                "schedule={SUNDAY=sleep all day long, MONDAY=go to the work, TUESDAY=read book, " +
                "WEDNESDAY=play football, THURSDAY=go to kfc, FRIDAY=participate in tennis, " +
                "SATURDAY=play video games}}, child=[], pet=[]}";
        Assert.assertEquals(expected, family.toString());
    }

    @Test
    public void testDeleteChild() {
        Family family = new Family(new Woman("Big", "Momma"), new Man("Big", "Poppa"));
        Human son = new Man("Ross", "Geller");
        Human son2 = new Woman("Chandler", "Bing");
        family.addChild(son);
        family.addChild(son2);
        family.deleteChild(son2);
        Human [] expectedArray = { son };
        Assert.assertArrayEquals(expectedArray, family.getChildren().toArray());
    }

    @Test
    public void testDeleteChildByIndex(){
        Family family = new Family(new Woman("Big", "Momma"), new Man("Big", "Poppa"));
        Human son = new Man("Ross", "Geller");
        Human son2 = new Woman("Chandler", "Bing");
        family.addChild(son);
        family.addChild(son2);
        family.deleteChild(1);
        Human [] expectedArray = { son };
        Assert.assertArrayEquals(expectedArray, family.getChildren().toArray());
    }

    @Test
    public void testDeleteChild_If_WrongValue() {
        Family family = new Family(new Woman("Big", "Momma"), new Man("Big", "Poppa"));
        Human son = new Man("Ross", "Geller");
        Human son2 = new Woman("Chandler", "Bing");
        family.addChild(son);
        family.addChild(son2);
        family.deleteChild(new Human("Monica", "Bing"));
        Human [] expectedArray = { son, son2 };
        Assert.assertArrayEquals(expectedArray, family.getChildren().toArray());
    }

    @Test
    public void testAddChild() {
        Family family = new Family(
                new Human("Lilly", "Potter"),
                new Human("James", "Potter")
        );
        Human son = new Human("Harry", "Potter");
        family.addChild(son);
        assertEquals(1, family.getChildren().size());
        assertEquals(son, family.getChildren().get(0));
    }

    @Test
    public void testCountFamily() {
        Family family = new Family(
                new Human("Rhaegar", "Targaryen"),
                new Human("Daenerys", "Targaryen")
        );
        Human son = new Man("Jon", "Snow");
        family.addChild(son);
        assertEquals(3, family.countFamily(family));
    }

}