package homeworks;

import java.util.HashMap;
import java.util.LinkedHashMap;

public final class Woman extends Human {

    Woman (String name, String surname) {
        super(name, surname);
    }

    Woman (String name, String surname, int year, int iq, LinkedHashMap<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    Woman (String name, String surname, int year, int iq, LinkedHashMap<String, String> schedule, Family family) {
        super(name, surname, year, iq, schedule, family);
    }

    @Override
    String greetPet() {
        if (this.family != null && this.family.getPet() != null) {
            return "Привет, " + this.family.getPet() + ", ты голодный?";
        } else {
            return "У меня нет питомца";
        }
    }

    String drinkWine () {
        return "wow! this wine-bottle is empty";
    }
}
