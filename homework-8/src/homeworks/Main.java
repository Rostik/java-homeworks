package homeworks;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Random;

public class Main {

    private static int getRandomNum () {
        Random random = new Random();
        return random.nextInt(101);
    }
    public static void main(String[] args) {
        Dog doggy = new Dog("Beebop",
                3, Main.getRandomNum(),
                Species.BULLDOG
        );
        Fish fish = new Fish("Nemo",
                1, Main.getRandomNum(),
                Species.CLOWN
        );
        Man dad = new Man("Robert", "Docker", 1975,  Main.getRandomNum(),
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to work");
                    put(DayOfWeek.TUESDAY.name(), "read book");
                    put(DayOfWeek.WEDNESDAY.name(), "go to the swimming pool");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play video games");
                }}
                );
        Woman mom = new Woman(
                "Amanda", "Docker", 1979, Main.getRandomNum(),
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to work");
                    put(DayOfWeek.TUESDAY.name(), "do some stuff");
                    put(DayOfWeek.WEDNESDAY.name(), "go to the market");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play with doggy");
                }}
        );
        Man son = new Man(
                "Andy", "Docker", 2001, Main.getRandomNum(),
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to school");
                    put(DayOfWeek.TUESDAY.name(), "read book");
                    put(DayOfWeek.WEDNESDAY.name(), "play football");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play video games");
                }}
        );
        Man son2 = new Man(
                "Mikey", "Docker", 2002, Main.getRandomNum(),
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to work");
                    put(DayOfWeek.TUESDAY.name(), "read book");
                    put(DayOfWeek.WEDNESDAY.name(), "play guitar");
                    put(DayOfWeek.THURSDAY.name(), "go to another fast-food");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play video games");
                }}
        );
        Family fam = new Family(mom, dad);

        fam.addChild(son);
        fam.addChild(son2);

        System.out.println(doggy.eat());
        System.out.println(doggy.respond());
        System.out.println(doggy.foul());
        doggy.setHabits("barking");
        doggy.setHabits("long-sleeping");
        System.out.println(doggy.toString());
        System.out.println(fish.toString());

        System.out.println(son.greetPet());
        System.out.println(son.describePet());
        System.out.println(mom.toString());

        System.out.println(mom.hashCode());
        System.out.println(fam.hashCode());
        System.out.println(dad.equals(mom));

        System.out.println(fam.countFamily(fam));
        fam.deleteChild(son);
        System.out.println(fam.countFamily(fam));
        System.out.println(fam.toString());
        fam.deleteChild(0);
        System.out.println(fam.countFamily(fam));
        son2.setFamily(fam);
        System.out.println(fam.toString());
        fam.addPet(doggy);
        fam.addPet(fish);
        System.out.println(fam.getPet());
        System.out.println(fam.getEveryPet("nickname"));
        System.out.println(dad.describePet());
        System.out.println(dad.eatNuggets());
        System.out.println(mom.drinkWine());

//        for (int i = 0; i < 500000; i++) {
//            new homeworks.Human("Neo", "Matrix", 1919, homeworks.Main.getRandomNum(),
//                    new String[][]{
//                            {"Sunday", "sleep all day long"},
//                            {"Monday", "take pill"},
//                            {"Tuesday", "read book"},
//                            {"Wednesday", "go to the swimming pool"},
//                            {"Thursday", "go to kfc"},
//                            {"Friday", "participate in music"},
//                            {"Saturday", "play video games"}
//                    }
//            );
//        }
    }
}