package homeworks;

import java.util.HashMap;
import java.util.LinkedHashMap;

public final class Man extends Human {

    Man (String name, String surname) {
        super(name, surname);
    }

    Man (String name, String surname, int year, int iq, LinkedHashMap<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    Man (String name, String surname, int year, int iq, LinkedHashMap<String, String> schedule, Family family) {
        super(name, surname, year, iq, schedule, family);
    }

    @Override
    String greetPet() {
        if (this.family != null && this.family.getPet() != null) {
            return "Привет, " + this.family.getPet() + ", я дома";
        } else {
            return "У меня нет питомца";
        }
    }

    String eatNuggets () {
        return "daaamn! u ate 9 nuggets";
    }
}
