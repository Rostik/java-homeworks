package homeworks;

import java.util.HashSet;

class DomesticCat extends Pet implements Foulable {

    DomesticCat (String nickname, int age, int trickLevel, Species species) {
        super(nickname, age, trickLevel);
        this.species = species;
    };

    String respond () {
        return "Привет, хозяин. Я - " + this.nickname + ", где моя еда?";
    }

    @Override
    public String foul() {
        return "Нужно хорошо замести следы...";
    }
}