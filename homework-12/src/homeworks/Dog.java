package homeworks;

public class Dog extends Pet implements Foulable {

    public Dog (String nickname, int age, int trickLevel, Species species) {
        super(nickname, age, trickLevel);
        this.species = species;
    };

    String respond () {
        return "Привет, хозяин. Я - " + this.nickname + ", я соскучился!";
    }

    @Override
    public String foul() {
        return "Нужно хорошо замести следы...";
    }
}
