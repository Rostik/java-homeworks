package homeworks;
import homeworks.Controllers.FamilyController;
import java.util.*;
import java.util.function.Predicate;

public class ConsoleApp {

    ArrayList<String> commandOptions = new ArrayList<String>() {{
        add("1. Заполнить тестовыми данными (автоматом создать несколько семей и сохранить их в базе)");
        add("2. Отобразить весь список семей (отображает список всех семей с индексацией, начинающейся с 1)");
        add("3. Отобразить список семей, где количество людей больше заданного");
        add("4. Отобразить список семей, где количество людей меньше заданного");
        add("5. Подсчитать количество семей, где количество членов равно");
        add("6. Создать новую семью");
        add("7. Удалить семью по индексу семьи в общем списке");
        add("8. Редактировать семью по индексу семьи в общем списке ");
        add("9. Удалить всех детей старше возраста (во всех семьях удаляются дети старше указанного возраста - будем считать, что они выросли)");
    }};

    FamilyController familyController = new FamilyController();
    Scanner in = new Scanner(System.in);
    Scanner externalIn = new Scanner(System.in);
    Boolean isExit = false;

    public void startConsoleApp () {

        commandOptions.forEach(System.out::println);
        System.out.println("Enter any number from the list (without dot or any other symbol): ");
        System.out.println("Or enter 'exit' to leave :)");

        do {
            String mainMenu = externalIn.nextLine().trim().toUpperCase();

            switch (mainMenu) {
                case ("1"): {
                    createTestFamilies();
                    break;
                }
                case ("2"): {
                    displayFamilies();
                    break;
                }
                case ("3"): {
                    getFamiliesBiggerThan();
                    break;
                }
                case ("4"): {
                    getFamiliesLessThan();
                    break;
                }
                case ("5"): {
                    getEqualFamilies();
                    break;
                }
                case ("6"): {
                    createNewFamily();
                    break;
                }
                case ("7"): {
                    deleteFamilyByIndex();
                    break;
                }
                case ("8"): {
                    editFamilyByIndex();
                    break;
                }
                case ("9"): {
                    deleteAllChildOlderThan();
                    break;
                }
                case ("EXIT"): {
                    isExit = true;
                }
                default:
                    System.out.println("There is no such option");
            }

        } while (!isExit);
        System.out.println("Goodbye :)");
    }

    private void createTestFamilies() {
        Dog doggy = new Dog("Beebop",
                3, 77,
                Species.BULLDOG
        );
        Fish fish = new Fish("Nemo",
                1, 65,
                Species.CLOWN
        );
        Man dad = new Man("Robert", "Docker", "15/11/1963", 87,
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to work");
                    put(DayOfWeek.TUESDAY.name(), "read book");
                    put(DayOfWeek.WEDNESDAY.name(), "go to the swimming pool");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play video games");
                }}
        );
        Woman mom = new Woman(
                "Amanda", "Docker", "03/12/1965", 103,
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to work");
                    put(DayOfWeek.TUESDAY.name(), "do some stuff");
                    put(DayOfWeek.WEDNESDAY.name(), "go to the market");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play with doggy");
                }}
        );
        Man son = new Man(
                "Andy", "Docker", "01/01/1999", 105,
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to school");
                    put(DayOfWeek.TUESDAY.name(), "read book");
                    put(DayOfWeek.WEDNESDAY.name(), "play football");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play video games");
                }}
        );
        Man son2 = new Man(
                "Mikey", "Docker", "15/05/2000", 87,
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to work");
                    put(DayOfWeek.TUESDAY.name(), "read book");
                    put(DayOfWeek.WEDNESDAY.name(), "play guitar");
                    put(DayOfWeek.THURSDAY.name(), "go to another fast-food");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play video games");
                }}
        );
        Family fam = new Family(mom, dad);
        fam.addChild(son);
        fam.addChild(son2);
        fam.addPet(doggy);

        Man dad2 = new Man("Derek", "Thompson", "03/09/1981", 97,
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to work");
                    put(DayOfWeek.TUESDAY.name(), "read book");
                    put(DayOfWeek.WEDNESDAY.name(), "go to the swimming pool");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play video games");
                }}
        );
        Woman mom2 = new Woman(
                "Natalie", "Portman", "03/09/1984", 88,
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to work");
                    put(DayOfWeek.TUESDAY.name(), "do some stuff");
                    put(DayOfWeek.WEDNESDAY.name(), "go to the market");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play with doggy");
                }}
        );

        Woman daughter = new Woman(
                "Scarlet", "Thompson", "12/12/2012", 81,
                new LinkedHashMap<String, String>() {{
                    put(DayOfWeek.SUNDAY.name(), "sleep all day long");
                    put(DayOfWeek.MONDAY.name(), "go to work");
                    put(DayOfWeek.TUESDAY.name(), "do some stuff");
                    put(DayOfWeek.WEDNESDAY.name(), "go to the market");
                    put(DayOfWeek.THURSDAY.name(), "go to kfc");
                    put(DayOfWeek.FRIDAY.name(), "participate in music");
                    put(DayOfWeek.SATURDAY.name(), "play with doggy");
                }}
        );
        Family fam2 = new Family(mom2, dad2);
        fam2.addChild(daughter);
        fam2.addPet(fish);
        System.out.println("Families created");
    }

    private void displayFamilies() {
        familyController.displayAllFamilies();
    }

    private void getFamiliesBiggerThan() {
        try {
            System.out.print("Введите число: ");
            int number = in.nextInt();
            Predicate<Family> familyPredicate = f -> f.countFamily() > number;
            boolean familyCondition = familyController.getAllFamilies().stream().noneMatch(familyPredicate);

            if (familyController.getAllFamilies().size() != 0) {
                if (familyCondition) {
                    System.out.println("There is no such family");
                } else {
                    familyController.getFamiliesBiggerThan(number);
                }
            } else {
                System.out.println("There are no families in the database");
            }
        } catch (InputMismatchException e) {
            in.nextLine();
            System.out.println("Ошибка: Вероятно вы ввели строку, где ожидалось число");
            commandOptions.forEach(System.out::println);
        }
    }

    private void getFamiliesLessThan() {
        try {
            System.out.print("Введите число: ");
            int number = in.nextInt();
            Predicate<Family> familyPredicate = f -> f.countFamily() < number;

            boolean familyCondition = familyController.getAllFamilies().stream().noneMatch(familyPredicate);
            if (familyController.getAllFamilies().size() != 0) {
                if (familyCondition) {
                    System.out.println("There is no such family");
                } else {
                    familyController.getFamiliesLessThan(number);
                }
            } else {
                System.out.println("There are no families in the database");
            }
        } catch (InputMismatchException e) {
            in.nextLine();
            System.out.println("Ошибка: Вероятно вы ввели строку, где ожидалось число");
            commandOptions.forEach(System.out::println);
        }
    }

    private void getEqualFamilies() {
        try {
            System.out.print("Введите число: ");
            int number = in.nextInt();
            Predicate<Family> familyPredicate = f -> f.countFamily() == number;
            boolean familyCondition = familyController.getAllFamilies().stream().noneMatch(familyPredicate);
            if (familyController.getAllFamilies().size() != 0) {
                if (familyCondition) {
                    System.out.println("There is no such family");
                } else {
                    familyController.getAllFamilies()
                            .stream()
                            .filter(family -> family.countFamily() == number)
                            .forEach(System.out::println);
                }
            } else {
                System.out.println("There are no families in the database");
            }
        } catch (InputMismatchException e) {
            in.nextLine();
            System.out.println("Ошибка: Вероятно вы ввели строку, где ожидалось число");
            commandOptions.forEach(System.out::println);
        }
    }

    private void createNewFamily() {
        try {
            System.out.print("Введите имя матери: ");
            String motherName = in.nextLine().trim();
            if (motherName.matches("\\d+")) {
                System.out.println("Shouldn't contain a number");
                return;
            }
            System.out.print("Введите фамилию матери: ");
            String motherSurname = in.nextLine().trim();
            if (motherSurname.matches("\\d+")) {
                System.out.println("Shouldn't contain a number");
                return;
            }
            System.out.print("Введите год рождения матери: ");
            int motherBirthYear = in.nextInt();
            System.out.print("Введите месяц рождения матери: ");
            int motherBirthMonth = in.nextInt();
            System.out.print("Введите день рождения матери: ");
            int motherBirthDay = in.nextInt();
            System.out.print("Введите IQ матери: ");
            int motherIq = in.nextInt();
            String motherBirthDate = "" + motherBirthDay + "/" + motherBirthMonth + "/" + motherBirthYear + "";

            Woman mom = new Woman(motherName, motherSurname, motherBirthDate, motherIq);

            in.nextLine();

            System.out.print("\nВведите имя отца: ");
            String fatherName = in.nextLine().trim();
            if (fatherName.matches("\\d+")) {
                System.out.println("Shouldn't contain a number");
                return;
            }
            System.out.print("Введите фамилию отца: ");
            String fatherSurname = in.nextLine().trim();
            if (fatherSurname.matches("\\d+")) {
                System.out.println("Shouldn't contain a number");
                return;
            }
            System.out.print("Введите год рождения отца: ");
            int fatherBirthYear = in.nextInt();
            System.out.print("Введите месяц рождения отца: ");
            int fatherBirthMonth = in.nextInt();
            System.out.print("Введите день рождения отца: ");
            int fatherBirthDay = in.nextInt();
            System.out.print("Введите IQ отца: ");
            int fatherIq = in.nextInt();
            String fatherBirthDate = "" + fatherBirthDay + "/" + fatherBirthMonth + "/" + fatherBirthYear + "";

            Man dad = new Man(fatherName, fatherSurname, fatherBirthDate, fatherIq);

            familyController.createNewFamily(dad, mom);
            System.out.println("Family is created");
        } catch (InputMismatchException e) {
            in.nextLine();
            System.out.println("Ошибка: Вероятно вы ввели строку, где ожидалось число");
            commandOptions.forEach(System.out::println);
        }
    }

    private void deleteFamilyByIndex() {
        try {
            System.out.print("Введите число: ");
            int number = in.nextInt();
            familyController.deleteFamilyByIndex(number);
            System.out.println("Family is deleted");
        } catch (InputMismatchException e) {
            in.nextLine();
            System.out.println("Ошибка: Вероятно вы ввели строку, где ожидалось число");
            commandOptions.forEach(System.out::println);
        }
    }

    private void editFamilyByIndex() {
        System.out.println("1. Родить ребенка");
        System.out.println("2. Усыновить ребенка");
        System.out.println("3. Вернуться в главное меню");
        String chooseNum = in.nextLine();
        if (!chooseNum.matches("\\d+")) {
            System.out.println("You have to pick a number");
            return;
        }
        try {
            switch (chooseNum) {
                case ("1"): {
                    System.out.print("Введите ID семьи: ");
                    int familyIndex = in.nextInt();
                    in.nextLine();
                    System.out.print("Введите имя девочки: ");
                    String womanName = in.nextLine();
                    if (womanName.matches("\\d+")) {
                        System.out.println("Shouldn't contain a number");
                        return;
                    }
                    System.out.print("Введите имя мальчика: ");
                    String manName = in.nextLine();
                    if (manName.matches("\\d+")) {
                        System.out.println("Shouldn't contain a number");
                        return;
                    }
                    Family family = familyController.getFamilyById(familyIndex);
                    familyController.bornChild(family, womanName, manName);
                    System.out.println("Поздравляем у вас родился ребенок :)");
                    break;
                }
                case ("2"): {
                    System.out.print("Введите ID семьи: ");
                    int familyIndex = in.nextInt();
                    in.nextLine();
                    System.out.println("Введите данные ребенка");
                    System.out.print("Введите имя: ");
                    String childName = in.nextLine();
                    if (childName.matches("\\d+")) {
                        System.out.println("Shouldn't contain a number");
                        return;
                    }
                    System.out.print("Введите фамилию: ");
                    String childSurname = in.nextLine();
                    if (childSurname.matches("\\d+")) {
                        System.out.println("Shouldn't contain a number");
                        return;
                    }
                    System.out.print("Введите дату рождения (формат - DD/MM/YYYY): ");
                    String childBirthDate = in.nextLine();
                    if (!childBirthDate.matches("\\d{2}/\\d{2}/\\d{4}")) {
                        System.out.println("Follow the format");
                        return;
                    }
                    System.out.print("Введите iq: ");
                    int childIq = in.nextInt();
                    Human child = new Human(childName, childSurname, childBirthDate, childIq);
                    Family family = familyController.getFamilyById(familyIndex);
                    familyController.adoptChild(family, child);
                    System.out.println("Поздравляем вы усыновили ребенка :)");
                    break;
                }
                case ("3"): {
                    commandOptions.forEach(System.out::println);
                    break;
                }
                default:
                    System.out.println("There is no such option");
                    break;
            }
        } catch (InputMismatchException | IndexOutOfBoundsException e) {
            in.nextLine();
            System.out.println("Ошибка: Вероятно вы ошиблись с ID семьи или ввели строку, где ожидалось число");
            commandOptions.forEach(System.out::println);
        } catch (FamilyOverflowException e) {
            System.out.println("Исключение: вы не можете родить/усыновить ребенка, когда в семье 5 и более человек");
        }
    }

    private void deleteAllChildOlderThan() {
        try {
            System.out.print("Введите возраст: ");
            int age = in.nextInt();
            familyController.deleteAllChildrenOlderThen(age);
            System.out.println("Это очень странно, но если такие дети были, то вы их удалили, поздравляем :)");
        } catch (InputMismatchException e) {
            in.nextLine();
            System.out.println("Ошибка: Вероятно вы ввели строку, где ожидалось число");
            commandOptions.forEach(System.out::println);
        }
    }
}
