package homeworks;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Human {

    String name;
    String surname;
    long birthDate;
    int iq;
    private Map<String, String> schedule;
    Family family;

    public Human (String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Human (String name, String surname, String birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = stringDateToMilliseconds(birthDate);
        this.iq = iq;
    }

    public Human (String name, String surname, String birthDate, int iq, LinkedHashMap<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = stringDateToMilliseconds(birthDate);
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human (String name, String surname, String birthDate, int iq, LinkedHashMap<String, String> schedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.birthDate = stringDateToMilliseconds(birthDate);
        this.iq = iq;
        this.schedule = schedule;
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public long getBirthday() {
        return birthDate;
    }

    public int getIq() {
        return iq;
    }

    Family getFamily() {
        return family;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    void setFamily(Family family) {
        this.family = family;
    }

    static long stringDateToMilliseconds (String date) {
        long milliseconds = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date localDate = formatter.parse(date);
            milliseconds = localDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return milliseconds;
    }

    private String millisecondsToStringDate () {
       return Instant.ofEpochMilli(this.birthDate)
                .atZone(ZoneId.of("Europe/Kiev"))
                .toLocalDateTime()
                .format(
                        DateTimeFormatter
                                .ofPattern("dd/MM/yyyy")
                );
    }

    String describeAge () {

        String [] dateArr = millisecondsToStringDate().split("/");
        int day = Integer.parseInt(dateArr[0]);
        int month = Integer.parseInt(dateArr[1]);
        int year = Integer.parseInt(dateArr[2]);
        LocalDate today = LocalDate.now();
        LocalDate birthday = LocalDate.of(year, month, day);

        Period period = Period.between(birthday, today);

        return "User age is: years - " + period.getYears() + ", " +
                "months - " + period.getMonths() + ", " +
                "days - " + period.getDays();
    }

    String greetPet() {
        if (this.family != null && this.family.getPet() != null) {
            return "Привет, " + this.family.getEveryPet("nickname");
        } else {
            return "У меня нет пса";
        }
    }

    String describePet() {
        if (this.family != null && this.family.getPet() != null) {
            return "У меня есть питомец/питомцы: " + this.family.getEveryPet("nickname") +
                    "ему/им: " + this.family.getEveryPet("age") + "он/они: " +
                    this.family.getEveryPet("trickLevel");
        } else {
            return "У меня нет питомца";
        }
    }

    public String prettyFormat(Family fam) {
        if (fam.getChildren().contains(this)) {
            if (this instanceof Man) {
                return "               boy: {name='" + this.name +
                        "', " + "surname='" + this.surname +
                        "', " + "birthDate='" + millisecondsToStringDate() +
                        "', " + "iq=" + this.iq + ", " +
                        (this.schedule != null ? "schedule=" + this.schedule.toString() + "}\n" : "schedule=" + null + "}\n");
            } else {
                return "               girl: {name='" + this.name +
                        "', " + "surname='" + this.surname +
                        "', " + "birthDate='" + millisecondsToStringDate() +
                        "', " + "iq=" + this.iq + ", " +
                        (this.schedule != null ? "schedule=" + this.schedule.toString() + "}\n" : "schedule=" + null + "}\n");
            }
        } else {
            return "{name='" + this.name +
                    "', " + "surname='" + this.surname +
                    "', " + "birthDate='" + millisecondsToStringDate() +
                    "', " + "iq=" + this.iq + ", " +
                    (this.schedule != null ? "schedule=" + this.schedule.toString() + "}" : "schedule=" + null + "}");
        }
    }

    @Override
    public String toString() {
        return "Human{name=" + this.name +
                ", surname=" + this.surname +
                ", birthDay=" +
                millisecondsToStringDate() +
                ", iq=" + this.iq + ", " +
                (this.schedule != null ? "schedule=" + this.schedule.toString() + "}" : "schedule=" + null + "}");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                iq == human.iq &&
                name.equals(human.name) &&
                surname.equals(human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Deleted obj: " + this.name + ", " + this.surname + "; It's hashcode: " + this.hashCode() );
        super.finalize();
    }
}
