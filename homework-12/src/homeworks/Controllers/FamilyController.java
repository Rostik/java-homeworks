package homeworks.Controllers;

import homeworks.Family;
import homeworks.FamilyOverflowException;
import homeworks.Human;
import homeworks.Pet;
import homeworks.Services.FamilyService;

import java.util.HashSet;
import java.util.List;

public class FamilyController {

    private final static FamilyService familyService = new FamilyService();

    public static void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public void getFamiliesBiggerThan(int members) {
        familyService.getFamiliesBiggerThan(members);
    }

    public void getFamiliesLessThan(int members) {
        familyService.getFamiliesLessThan(members);
    }

    public Integer countFamiliesWithMemberNumber(int members) {
        return familyService.countFamiliesWithMemberNumber(members);
    }

    public void createNewFamily(Human man, Human woman) {
        familyService.createNewFamily(man, woman);
    }

    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String womanName, String manName) throws FamilyOverflowException {
        if (family.countFamily() >= 5) {
            throw new FamilyOverflowException("Вы не можете родить ребенка, когда в семье 5 и более человек");
        }
        return familyService.bornChild(family, womanName, manName);
    }

    public Family adoptChild(Family family, Human child) throws FamilyOverflowException {
        if (family.countFamily() >= 5) {
            throw new FamilyOverflowException("Вы не можете усыновить ребенка, когда в семье 5 и более человек");
        }
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public Integer count() {
        return familyService.count();
    }

    public Family getFamilyById(int id) {
        return familyService.getFamilyById(id);
    }

    public HashSet<Pet> getPets(int id) {
        return familyService.getPets(id);
    }

    public void addPet(int id, Pet pet) {
        familyService.addPet(id, pet);
    }

}
