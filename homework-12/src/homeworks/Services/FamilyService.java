package homeworks.Services;

import homeworks.*;
import homeworks.DAO.CollectionFamilyDao;
import homeworks.DAO.FamilyDao;

import java.time.Year;
import java.util.*;
import java.util.stream.Collectors;

public class FamilyService {

    private final FamilyDao familyDao = new CollectionFamilyDao();

    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() { familyDao.getAllFamilies().forEach(e -> System.out.println((familyDao.getAllFamilies().indexOf(e) + 1) + ". " + e.prettyFormat())); }

    public void getFamiliesBiggerThan(int members) {
        familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() > members)
                .forEach(e -> System.out.println(e.prettyFormat()));
    }

    public void getFamiliesLessThan(int members) {
        familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() < members)
                .forEach(e -> System.out.println(e.prettyFormat()));
    }

    public Integer countFamiliesWithMemberNumber(int members) {
        long countedFamilies = familyDao.getAllFamilies()
                .stream()
                .filter(e -> e.countFamily() == members)
                .count();
        return (int) countedFamilies;
    }

    public void createNewFamily(Human man, Human woman) {
        saveFamily(new Family(man, woman));
    }

    public void deleteFamilyByIndex(int index) {
        familyDao.deleteFamily(index - 1);
    }

    public Family bornChild(Family family, String womanName, String manName) {
        Random random = new Random();
        int gender = random.nextInt(4);
        int iq = random.nextInt(101);
        if (gender >= 2) {
            family.addChild(new Woman(womanName, family.father.getSurname(), "21/03/2020", iq, new LinkedHashMap<>(), family));
        } else {
            family.addChild(new Man(manName, family.father.getSurname(), "20/01/2020", iq, new LinkedHashMap<>(), family));
        }
        saveFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyDao.getAllFamilies().forEach(e -> {
                    e.setChildren(
                            e.getChildren()
                                    .stream()
                                    .filter(child -> {
                                        Calendar c = Calendar.getInstance();
                                        c.setTimeInMillis(child.getBirthday());
                                        int year = c.get(Calendar.YEAR);
                                        return (Year.now().getValue() - year) < age;
                                    })
                                    .collect(Collectors.toList())
                    );
                    saveFamily(e);
                }
        );
    }

    public Integer count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int id) {
        return familyDao.getAllFamilies().get(id - 1);
    }

    public HashSet<Pet> getPets(int id) {
        return familyDao.getAllFamilies().get(id).getPet();
    }

    public void addPet(int id, Pet pet) {
        Family family = familyDao.getAllFamilies().get(id);
        family.addPet(pet);
        saveFamily(family);
    }

}