//        С помощью java.util.Random программа загадывает случайное число в диапазоне [0-100] и предлагает игроку через консоль ввести свое имя, которое сохраняется в переменной name.
//        Перед началом на экран выводится текст: Let the game begin!.
//        Сам процесс игры обрабатывается в бесконечном цикле.
//        Игроку предлагается ввести число в консоль, после чего программа сравнивает загаданное число с тем, что ввел пользователь.
//        Если введенное число меньше загаданного, то программа выводит на экран текст: Your number is too small. Please, try again.. Если введенное число больше загаданного, то программа выводит на экран текст: Your number is too big. Please, try again..
//        Если введенное число соответствуют загаданному, то программа выводит текст: Congratulations, {name}!.
//        Задание должно быть выполнено ипспользуя массивы (НЕ используйте интерфейсы List, Set, Map).

package com.danit.homeworks;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    private static void chooseNum () {

        Random random = new Random();
        int pickedNum = random.nextInt(101);

        System.out.println(pickedNum);

        System.out.println("Let the game begin!");

        Scanner name = new Scanner(System.in);
        System.out.print("Enter your name: ");
        String enteredName = name.nextLine();

        Scanner number = new Scanner(System.in);
        System.out.print("Try to guess prescribed number: ");
        int enteredNumber = number.nextInt();

        while (true) {
            if (pickedNum == enteredNumber){
                System.out.print("Congratulations, " + enteredName + " !");
                break;
            } else if (pickedNum < enteredNumber) {
                System.out.print("Your number is too big. Please, try again..");
                enteredNumber = number.nextInt();
            } else if (pickedNum > enteredNumber) {
                System.out.print("Your number is too small. Please, try again..");
                enteredNumber = number.nextInt();
            }
        }


    }

    public static void main(String[] args) {
        chooseNum();
    }
}
